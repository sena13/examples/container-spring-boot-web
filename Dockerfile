# Dockerfile
FROM openjdk:11.0.11-jre

# jar 파일 복사
COPY webapp-spring-boot.jar /webapp-spring-boot.jar

ENTRYPOINT ["java", "-Dspring.profiles.active=dev", "-jar", "/webapp-spring-boot.jar"]